
module App {
    requires SayHello;
    requires SayHey;
    requires SayYo;

    uses com.alchemistfrog.sayhello.SayHelloInterface;
}
