package com.alchemistfrog.app;

import com.alchemistfrog.sayhello.SayHelloInterface;
import java.util.ServiceLoader;

/**
 *
 * @author Romain Ducreux <rducreux@pm.me>
 */
public class App {

    public static void main(String[] args) {
        Iterable<SayHelloInterface> services = ServiceLoader.load(SayHelloInterface.class);

        for (SayHelloInterface sayhello : services) {
            sayhello.sayHello();
        }
    }
}
