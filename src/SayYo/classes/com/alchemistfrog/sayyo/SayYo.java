package com.alchemistfrog.sayyo;

import com.alchemistfrog.sayhello.SayHelloInterface;

/**
 *
 * @author Romain Ducreux <rducreux@pm.me>
 */
public class SayYo implements SayHelloInterface {

    @Override
    public void sayHello() {
        System.out.println("Yo!");
    }

}
