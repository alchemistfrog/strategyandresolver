
module SayYo {
    requires SayHello;

    exports com.alchemistfrog.sayyo;

    provides com.alchemistfrog.sayhello.SayHelloInterface
            with com.alchemistfrog.sayyo.SayYo;

}
