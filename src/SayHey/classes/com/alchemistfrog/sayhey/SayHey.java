package com.alchemistfrog.sayhey;

import com.alchemistfrog.sayhello.SayHelloInterface;

/**
 *
 * @author Romain Ducreux <rducreux@pm.me>
 */
public class SayHey implements SayHelloInterface {

    @Override
    public void sayHello() {
        System.out.println("Hey!");
    }

}
