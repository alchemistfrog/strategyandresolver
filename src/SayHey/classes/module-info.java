
module SayHey {
    requires SayHello;

    exports com.alchemistfrog.sayhey;

    provides com.alchemistfrog.sayhello.SayHelloInterface
        with com.alchemistfrog.sayhey.SayHey;
}
