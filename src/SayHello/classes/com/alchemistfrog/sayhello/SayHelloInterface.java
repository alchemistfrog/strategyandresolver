package com.alchemistfrog.sayhello;

/**
 *
 * @author Romain Ducreux <rducreux@pm.me>
 */
public interface SayHelloInterface {

    public void sayHello();
}
